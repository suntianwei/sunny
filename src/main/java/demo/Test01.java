package demo;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.FilePipeline;
import us.codecraft.webmagic.processor.PageProcessor;

public class Test01 implements PageProcessor {
    @Override
    public void process(Page page) {
        page.addTargetRequests(page.getHtml().xpath("//div[@class='l_title']/a").links().all());
        page.putField("title",page.getHtml().xpath("//h1[@class='title']").all());
        page.putField("time",page.getHtml().xpath("//div[@class='art_time']").regex("日期:").all());
        page.putField("context",page.getHtml().xpath("//div[@class='content']/p").all());
        page.putField("pic",page.getHtml().xpath("//div[@class='content']/p/img").all());
    }

    private Site site=Site.me().
            setCharset("GBK").
            setRetrySleepTime(10000).
            setRetryTimes(1000).
            setSleepTime(3);
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        Spider.create(new Test01()).
                addUrl("http://www.zuowen.net/yingyuzuowen/").
                addPipeline(new FilePipeline("result")).
                thread(50).
                run();
    }
}
